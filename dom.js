// Una etiqueta html es un elemento para javascript
// las etiquetas son un tipo de nodo. Puede haber nodos de texto, documento y de atributo y más node.nodeDeTexto, node.nodeDeDocumento, node.nodeDeAtributo. Un nodo de tipo elemento es una etiqueta de html en particular

console.log(document.getElementsByTagName("li"));
console.log(document.getElementsByClassName("card"));
console.log(document.getElementsByName("nombre"));
console.log(document.getElementById("menu"));
console.log(document.querySelector("#menu"));
console.log(document.querySelector("a"));
console.log(document.querySelectorAll("a"));
console.log(document.querySelectorAll("a").length);
document.querySelectorAll("a").forEach((el) => console.log(el));
console.log(document.querySelector(".card"));
console.log(document.querySelectorAll(".card"));
console.log(document.querySelectorAll(".card")[2]);
console.log(document.querySelector("#menu li"));
console.log(document.querySelectorAll("#menu li"));

console.log('------------------ATRIBUTOS Y DATA-ATRIBUTES----------------------------');
console.log(document.documentElement.lang);
console.log(document.documentElement.getAttribute("lang"));
console.log(document.querySelector(".link-dom").href);
console.log(document.querySelector(".link-dom").getAttribute("href"));

document.documentElement.lang = "en";
console.log(document.documentElement.lang);
document.documentElement.setAttribute("lang", "es-MX");
console.log(document.documentElement.lang);

const $linkDOM = document.querySelector(".link-dom");

$linkDOM.setAttribute("target", "_blank");
$linkDOM.setAttribute("rel", "noopener");
$linkDOM.setAttribute("href", "https://youtube.com/jonmircha");
console.log($linkDOM.hasAttribute("rel"));
$linkDOM.removeAttribute("rel");
console.log($linkDOM.hasAttribute("rel"));

//Data-Attributes
console.log($linkDOM.getAttribute("data-description"));
console.log($linkDOM.dataset);
console.log($linkDOM.dataset.description);
$linkDOM.setAttribute("data-description", "Modelo de Objeto del Documento");
console.log($linkDOM.dataset.description);
$linkDOM.dataset.description = "Suscríbete a mi canal y comparte";
console.log($linkDOM.dataset.description);
console.log($linkDOM.hasAttribute("data-id"));
$linkDOM.removeAttribute("data-id");
console.log($linkDOM.hasAttribute("data-id"));

console.log('------------------ESTILOS Y VARIABLES CSS----------------------------');
// Devuelve la declaración de las propiedades de los estilos css como background-color que están declarados en el elemento, no importa si se declaran después de esta linea de código, devuelve todas las propiedades aplicables al elemento, no importa si éstas estan vacias.
console.log($linkDOM.style);
$linkDOM.style.borderRadius = ".5rem";
// Hace lo mismo que el de arriba ($linkDOM.style) pero lo devuelve en cadena de texto y sólo devuelve los elementos aplicados antes de la declaración de la misma linea de texto y devuelve tambien los elemento que se aplicaron en linea en la etiqueta html
console.log($linkDOM.getAttribute("style"));
console.log($linkDOM.style.backgroundColor);
console.log($linkDOM.style.color);
// Devuelve todas las propiedades aplicables en orden alfabetico y también devuelve todos los estilos aplicados. Devuelve las propiedades que el navegador le asigna al elemento por defecto y las que uno mismo asigna. Devuelve los valores como los interpreta el navegador, no devuelve los valores como porcentajes por ejemplo, sino como pixeles
console.log(window.getComputedStyle($linkDOM));
console.log(getComputedStyle($linkDOM).getPropertyValue("color"));

$linkDOM.style.setProperty("text-decoration", "none");
$linkDOM.style.setProperty("display", "block");
$linkDOM.style.width = "50%";
$linkDOM.style.textAlign = "center";
$linkDOM.style.marginLeft = "auto";
$linkDOM.style.marginRight = "auto";
$linkDOM.style.padding = "1rem";
$linkDOM.style.borderRadius = ".5rem";

console.log($linkDOM.style);
console.log($linkDOM.getAttribute("style"));
console.log(getComputedStyle($linkDOM));

//Variables CSS - Custom Properties CSS
const $html = document.documentElement,
  $body = document.body;
// Obtener las variables que estan en :root dentro de la etiqueta style dentro de la etiqueta html.
let varDarkColor = getComputedStyle($html).getPropertyValue("--dark-color"),
  varYellowColor = getComputedStyle($html).getPropertyValue("--yellow-color");

console.log(varDarkColor, varYellowColor);
// Se le asigna a la etiqueta body en el background-color lo que habia en la variable css
$body.style.backgroundColor = varDarkColor;
$body.style.color = varYellowColor;
// Se reasigna el valor de la variable css llamada --dark-color
$html.style.setProperty("--dark-color", "#000");
// Se vuelve a obtener el valor de la variable css  que se reasignó y se asigna a la variable de javascript varDarkColor de nuevo
varDarkColor = getComputedStyle($html).getPropertyValue("--dark-color");
// Se vuelve a modificar el background-color de la etiqueta body
$body.style.setProperty("background-color", varDarkColor);

console.log('------------------CLASES CSS----------------------------');
const $card = document.querySelector(".card");

console.log($card);
// Devuelve una cadena de texto con el nombre de las clases separadas por espacios
console.log($card.className);
// Devuelve el nombre de las clases dentro de un DOMTokenList que es como un array. Esta devolviendo las clases aunque hayan sido agregadas despues con javascript
console.log($card.classList);
// Verifica si contiene esa clase
console.log($card.classList.contains("rotate-45"));
$card.classList.add("rotate-45");
console.log($card.classList.contains("rotate-45"));
console.log($card.className);
console.log($card.classList);
$card.classList.remove("rotate-45");
console.log($card.classList.contains("rotate-45"));
// Quita o pone esa clase dependiendo si la tiene o no. Si no la tiene la pone y si la tiene la quita
$card.classList.toggle("rotate-45");
console.log($card.classList.contains("rotate-45"));
$card.classList.toggle("rotate-45");
console.log($card.classList.contains("rotate-45"));
$card.classList.toggle("rotate-45");
// Reemplaza una clase por otra
$card.classList.replace("rotate-45", "rotate-135");
// Agrega dos clases
$card.classList.add("opacity-80", "sepia");
$card.classList.remove("opacity-80", "sepia");
// Quitar o poner esas dos clases dependiendo de si las tienen o no
$card.classList.toggle("opacity-80", "sepia");


console.log('------------------Texto y HTML----------------------------');

const $whatIsDOM = document.getElementById("que-es");
let text = `
    <p>
      El Modelo de Objetos del Documento (<b><i>DOM - Document Object Model</i></b>) es un API para documentos HTML y XML.
      Test.
    </p>
    <p>
      Éste proveé una representación estructural del documento, permitiendo modificar su contenido y presentación visual mediante código JS.
    </p>
    <p>
      <mark>El DOM no es parte de la especificación de JavaScript, es una API para los navegadores.</mark>
    </p>
  `;
// Agrega texto dentro de la etiqueta HTML, y lo pone como va, justo como está en el template string, con saltos de linea y toda la cosa. Tambien recupera el texto que haya pero sin las etiquetas como span, sólo recupera puro texto
// $whatIsDOM.innerText = text;
// Agrega el texto sin los saltos de linea y también recupera texto pero sin las etiquetas como span, sólo texto.Agrega texto sin interpretarlo como etiqueta html si esta escrito como <img> por ejemplo
// $whatIsDOM.textContent = text;
// Agrega HTML. El string con etiquetas html lo representa como una etiqueta al agregarlo.Tambien recupera todo lo que tiene esa etiqueta adentro devuelto en un string con las etiquetas html como en la variable text de arriba.
// $whatIsDOM.innerHTML = text;
// Recupera la etiqueta misma con todo su contenido dentro con las etiquetas que tiene también, todo esto lo devuelve en un string. Si se reemplaza como con la variable text que esta arriba reemplaza totalmente la etiqueta agregando nuevo contenido donde estaba esa etiqueta antes. En este caso va a agregar tres nuevos parrafos donde antes estaba la etiqueta seleccionada con id=que-es, la etiqueta con id=que-es desaparecerá totalmente
$whatIsDOM.outerHTML = text;

console.log('------------------DOM Traversing - Recorriendo el DOM----------------------------');

const $cards = document.querySelector(".cards");

console.log($cards);
// Devuelve una HTMLCollection, que es similar a un array, con todos los hijos nodos html
console.log($cards.children);
// Seleccionando sólo a un hijo de la HTMLCollection
console.log($cards.children[2]);
// Es de sólo lectura. Devuelve el padre del nodo actual.
console.log($cards.parentElement);
// Devuelve el primer nodo HTML hijo
console.log($cards.firstElementChild);
// Devuelve el último nodo HTML hijo
console.log($cards.lastElementChild);
// Devuelve el elemento hermano anterior
console.log($cards.previousElementSibling);
// Devuelve el elemento hermano siguiente
console.log($cards.nextElementSibling);
// Se supone que devuelve al elemento más cercano que se le indica pero que sea ancestro, si no, devuelve null
console.log($cards.closest("div"));
console.log($cards.closest("body"));
console.log($cards.children[3].closest("section"));

// Cambiando el color de fondo al nodo padre si es que existe
// if ($cards.parentElement) {
//   $cards.parentElement.style.backgroundColor = "red";
// }


console.log('------------------Creando Elementos y Fragmentos----------------------------');
const $figure = document.createElement("figure"),
  $img = document.createElement("img"),
  $figcaption = document.createElement("figcaption"),
  //createTextNode crea un nodo de texto que se agrega con appendChild, sirve para escapar etiquetas html como <img>
  $figcaptionText = document.createTextNode("Animals"),
  // $cards = document.querySelector(".cards"),
  $figure2 = document.createElement("figure");

$img.setAttribute("src", "https://placeimg.com/200/200/animals");
$img.setAttribute("alt", "Animals");
$figure.classList.add("card");
// Agregando el nodo de texto creado al figcaption
$figcaption.appendChild($figcaptionText);
// appendChild va agregando nuevos elementos secuencialmente sin borrar los anteriores
$figure.appendChild($img);
$figure.appendChild($figcaption);
$cards.appendChild($figure);

$figure2.innerHTML = `
<img src="https://placeimg.com/200/200/people" alt="People">
<figcaption>People</figcaption>
`;
$figure2.classList.add("card");
// Si algo es creado como nodo con createElement tiene que ser agregado con appendChild
$cards.appendChild($figure2);

const estaciones = ["Primavera", "Verano", "Otoño", "Invierno"],
  $ul = document.createElement("ul");

document.write("<h3>Estaciones del Año</h3>");
document.body.appendChild($ul);

estaciones.forEach((el) => {
  const $li = document.createElement("li");
  $li.textContent = el;
  $ul.appendChild($li);
});

const continentes = ["África", "América", "Asia", "Europa", "Oceanía"],
  $ul2 = document.createElement("ul");
  // $ul2.innerHTML = "";

document.write("<h3>Continentes del Mundo</h3>");
document.body.appendChild($ul2);
// $ul2.innerHTML = "";
continentes.forEach((el) => ($ul2.innerHTML += `<li>${el}</li>`));


const meses = [
    "Enero",
    "Febrero",
    "Marzo",
    "Abril",
    "Mayo",
    "Junio",
    "Julio",
    "Agosto",
    "Septiembre",
    "Octubre",
    "Noviembre",
    "Diciembre",
  ],
  $ul3 = document.createElement("ul"),
  // Creando un fragmento de documento con createDocumentFragment(). A un fragmento se le pueden meter otros elementos nodo hechos con createElement()
  $fragment = document.createDocumentFragment();

meses.forEach((el) => {
  const $li = document.createElement("li");
  $li.textContent = el;
  // Agregando Elementos nodo html al frangmento
  $fragment.appendChild($li);
});

document.write("<h3>Meses del Año</h3>");
// Añadiendo el fragmento al nodo padre. Sólo se añade lo que se le agregó al fragmento ya que en sí no hay una etiqueta fragmento o algo así. Sólo se agregan todas las etiquetas li a la etiqueta ul
$ul3.appendChild($fragment);
// Al body se le agrega la etiqueta ul
document.body.appendChild($ul3);


console.log('------------------Templates HTML----------------------------');
// .content returns a <template> element's template contents. .content Devuelve el contenido de plantilla de un elemento <template>. Sólo nos devuelve su contenido, todo lo que hay dentro de la etiqueta template y no la etiqueta misma, se usa para poder hacer un querySelector dentro del template, si no, marca error al hacer el querySelector
const $template = document.getElementById("template-card").content,
      $fragment2 = document.createDocumentFragment(),
      cardsContent = [
        {
          title: "Tecnología",
          img: "https://placeimg.com/200/200/tech",
        },
        {
          title: "Animales",
          img: "https://placeimg.com/200/200/animals",
        },
        {
          title: "Arquitectura",
          img: "https://placeimg.com/200/200/arch",
        },
        {
          title: "Gente",
          img: "https://placeimg.com/200/200/people",
        },
        {
          title: "Naturaleza",
          img: "https://placeimg.com/200/200/nature",
        },
      ];

    cardsContent.forEach((el) => {
      // Se hace un querySelector pero sólo dentro de la etiqueta figure que esta adentro de la etiqueta template y se le agregan atributos a img y se le agrega texto a figcaption. La etiqueta template no se visualiza en el navegador, el navegador no la toma en cuenta y no pinta nada de su contenido. Por eso se puso sólo para tomarla como referencia y modificar su contenido para irlo clonando.
      $template.querySelector("img").setAttribute("src", el.img);
      $template.querySelector("img").setAttribute("alt", el.title);
      $template.querySelector("figcaption").textContent = el.title;
// El true como segundo argumento en el importNode significa que va a clonar tambien toda la estructura interna del template, si no, sólo copiaría la etiqueta template de apertura y cierre sin nada de contenido dentro y al agregar sólo las etiquetas template no agregaría nada practicamente. Al agregar cada etiqueta template con su contenido dentro pintaría solamente el contenido de la etiqueta template y la etiqueta template misma desaparecería.
      let $clone = document.importNode($template, true);
      // Los nodos clonados se van agregando uno por uno al fragmento
      $fragment2.appendChild($clone);
    });
    // El fragmento se agrega al nodo
    $cards.appendChild($fragment2);

// Las etiquetas template no se renderizan, son como clases en programación para que apartir de ellas se empiecen a generar estructuras del DOM dinamicamente



console.log('------------------Modificando Elementos (Old Style)----------------------------');
const $newCard = document.createElement("figure"),
// cloneNode sirve para clonar el nodo actual, en este caso el nodo $cards será clonado y el argumento pasado en true indica que se deben clonar todos los hijos del nodo tambien, si no, sólo se clonaria el nodo actual sin contenido
      $cloneCards = $cards.cloneNode(true);

$newCard.innerHTML = `
  <img src="https://placeimg.com/200/200/any" alt="Any">
  <figcaption>Any</figcaption>
`;
$newCard.classList.add("card");

//$cards.replaceChild($newCard, $cards.children[2]);
//$cards.removeChild($cards.lastElementChild);
// Al section que tiene las tarjetas se le inserta un nuevo nodo($newCard) antes del primer nodo que tiene como hijo, que ya no será el primero.
$cards.insertBefore($newCard, $cards.firstElementChild);
// El nodo clonado se agrega al body del HTML
document.body.appendChild($cloneCards);


console.log('------------------Modificando Elementos (Cool Style)----------------------------');
/*
.insertAdjacent...
Inserta un elemento nodo, son los que se crean con createElement()
  .insertAdjacentElement(position, el)
Analiza la cadena de texto introducida como cadena HTML o XML e inserta al árbol DOM los nodos resultantes de dicho análisis en la posición especificada. <figcaption>Any</figcaption> lo combierte a HTML
  .insertAdjacentHTML(position, html)
It inserts a given text node at a given position relative to the element it is invoked upon. Inserta un nodo de texto dado en una posicion dada con respecto al elemento sobre el que se invoca. Recibe un string como segundo argumento
  .insertAdjacentText(position, text)

Posiciones:
  beforebegin(hermano anterior)
  afterbegin(primer hijo)
  beforeend(ultimo hijo)
  afterend(hermano siguiente)
*/

const $newCard2 = document.createElement("figure");

let $contenCard = `
  <img src="https://placeimg.com/200/200/any" alt="Any">
  <figcaption></figcaption>
`;
$newCard2.classList.add("card");

$newCard2.insertAdjacentHTML("afterbegin", $contenCard);
$cards.insertAdjacentElement("beforeend", $newCard2);
// Agregando texto con insertAdjacentText
$newCard2.querySelector("figcaption").insertAdjacentText("afterbegin", "Any");

// It inserts a set of Node objects or DOMString objects before the first child of the Element.
//$cards.prepend($newCard2);
//  It inserts a set of Node objects or DOMString objects after the last child of the Element.
//$cards.append($newCard2);
// Agrega un nodo elemento como hermano anterior. El nodo se agregará antes de $cards
//$cards.before($newCard2);
// Agrega un nodo elemento como hermano siguiente. El nodo se agregará despues de $cards
//$cards.after($newCard2);


console.log('------------------Manejadores de Eventos----------------------------');
// Las funciones que se ejecutan en un evento se llaman Event Handler (Manejador de Eventos).
function holaMundo() {
  alert("Hola Mundo");
  console.log(event);
}

function saludar(nombre = "Desconocid@") {
  alert(`Hola ${nombre}`);
  console.log(event);
}

const $eventoSemantico = document.getElementById("evento-semantico"),
  $eventoMultiple = document.getElementById("evento-multiple"),
  $eventoRemover = document.getElementById("evento-remover");
// La función que se le pasa al evento semantico debe ir sin parentesis porque si no se ejecutaria al momento de cargar la pagina.
$eventoSemantico.onclick = holaMundo;
// Sobreescribiendi la funcion manejadora de eventos semanticos. A las funciones manejadoras no se les puede pasar parametros. Sólo recibe el objecto evento
$eventoSemantico.onclick = function (e) {
  alert("Hola Mundo Manejador de Eventos Semántico");
  console.log(e);
  //Aun que no se ponga el evento como parametro se puede acceder a él con la palabra reservada event. Simpre se podra acceder con la palabra reservada event aunque se nombre al objeto diferente al pasarlo como argumento.
  console.log(event);
};
// La función se debe pasar sin los parentesis porque si no se ejecuta a la carga del documento
// Se les debe pasar la función sin los parentesis
$eventoMultiple.addEventListener("click", holaMundo);
$eventoMultiple.addEventListener("click", (e) => {
  alert("Hola Mundo Manejador de Eventos Múltiple");
  console.log(e);
  // Tipo de evento click
  console.log(e.type);
  // Objeto que lo origina
  console.log(e.target);
  console.log(event);
});
$eventoMultiple.addEventListener("click", () => {
  saludar();
  saludar("Rafael");
});

const removerDobleClick = (e) => {
  alert(`Removiendo el evento de tipo ${e.type}`);
  console.log(e);
  // Se le especifica que función escuchadora de eventos se va a eliminar puesto que puede tener muchas funciones manejadoras, como con los eventos multiples. En este caso eliminamos la función escuchadora removerDobleClick(). La función debe ser una función declarada fuera para poder eliminarla, como removerDobleClick()
  $eventoRemover.removeEventListener("dblclick", removerDobleClick);
  $eventoRemover.disabled = true;
};

$eventoRemover.addEventListener("dblclick", removerDobleClick);

console.log('------------------Flujo de Eventos (Burbuja y Captura)----------------------------');
// El método addEventListener() tiene un tercer párametro que puede ser de tipo boolean, si no se especifica, el valor por defecto es false.
// Si el parámetro es false se ejecuta la fase de burbuja (ir del elemento más interno al más externo).
// Si el parámetro es true se ejecuta la fase de captura (ir del elemento más externo al más interno).

// Trae todas la divs que estan dentro de la clase eventos-flujo. Dentro de eventos flujo hay una div que a su vez tiene otra div como hijo y ésta a su vez tiene otra div como hijo
const $divsEventos = document.querySelectorAll(".eventos-flujo div");

// function flujoEventos(e) {
//   // this hace referencia al elemento div actual, this equivale al elemento html actual en la funcion manejadora. className devuelve un string con todas las clases
//   // e.target es el Objeto (Elemento HTML) que originó el evento (En este caso de click).
//   console.log(
//     `Hola te saluda ${this.className}, el click lo originó ${e.target.className}`
//   );
// }

console.log($divsEventos);

// Por cada div que hay dentro de la clase eventos-flujo es asignado un escuchador de eventos.
// Como los tres tienen el mismo escuchador de ventos (flujoEventos()) Si se le da click al div más interno (El 3) también va a ejecutar flujoEventos en el div 2 y el 3 porque de alguna forma también se le dio click a esos elementos puesto que div 1 estaba dentro de ellos. Va a ejecutar primero la funcion manejadora del div más interno (El 1) y va a ir subiendo en el dom ejecutando los demás, despues div 2 y luego div 3.
// $divsEventos.forEach((div) => {
//   // Así se da la Fase de burbuja, la cual es por defecto
//   //div.addEventListener("click", flujoEventos);
//   //div.addEventListener("click", flujoEventos, false);

//   //Fase de captura. Se pasa como segundo argumento true para activar la fase de captura y las funciones controladoras van a ir activandose de la más externa a la más interna.
//   //div.addEventListener("click", flujoEventos, true);
//   div.addEventListener("click", flujoEventos, {
//     // Aquí tambien se puede indicar si se va a activar la fase de captura
//     capture: true,
//     // Con once el evento sólo se activa una sola vez. Se le quita el evento como con removeEventListener después de que el evento se activó la primera vez. Esto le quita el escuchador de eventos a los 3 elementos.
//     once: true,
//   });
// });


console.log('------------------stopPropagation & preventDefault----------------------------');
// const $divsEventos = document.querySelectorAll(".eventos-flujo div");

//  Seleccionando el elemento HTML <a> que está dentro de la clase eventos-flujo
const $linkEventos = document.querySelector(".eventos-flujo a");

function flujoEventos(e) {
  console.log(
    `Hola te saluda ${this}, el click lo originó ${e.target.className}`
  );
  // Se detiene la propagación de captura o burbuja y sólo se activa la función manejadora del elemento que originó el evento, no se activan las demás funciones manejadoras.
  e.stopPropagation();
}

// $divsEventos.forEach((div) => {
//   div.addEventListener("click", flujoEventos);
// });

// $linkEventos.addEventListener("click", (e) => {
//   console.log('Click al enlace');
//   // Se detiene el comportamiento por defecto del enlace que es abrir una pagina en la misma pestaña o en otra, creo que también detiene el comportamiento de mandar a una misma sección del documento como cuando se le pone href=#
//   e.preventDefault();
//   // Detiene la propagación porque se originó en el div 3, el enlace está dentro de div 3. Ya no activa todas las demas funciones controladoras puesto que el enlace esta dentro de todos los divs. Si se quita el stopPropagation de aquí sólo se va a activar la función controladora de div 3 puesto que es la más cercana y la tres va a detener la propagacion de las demás porque tiene un stopPropagation
//   e.stopPropagation();
// });


console.log('------------------Delegación de Eventos----------------------------');
// Se le asigna el escuchador de eventos tipo click al documento
document.addEventListener("click", (e) => {
  // Se comprueba si el elemento al que se le dio click coincide con el selector css que se le pasa a .matches, éste recibe un string. En este caso puede ser cualquier div dentro de la clase eventos-flujo
  if (e.target.matches(".eventos-flujo div")) {
    // Le pasamos el objeto evento
    flujoEventos(e);
  }

  if (e.target.matches(".eventos-flujo a")) {
    alert("Click al enlace");
    e.preventDefault();
    //e.stopPropagation();
  }
});


// BOM: Browser Object Model
// El Modelo de Objetos del Navegador es un API que permite acceder y modificar las propiedades de las ventanas del propio navegador.
// Mediante el BOM, es posible redimensionar y mover la ventana del navegador y realizar muchas otras manipulaciones no relacionadas con el contenido de la página HTML.
// El mayor inconveniente del BOM es que, a diferencia del DOM, no es un API estándar, cada navegador lo puede interpretar de diferente manera, sin embargo, en los últimos años los navegadores están más estandarizados entre sí.

console.log('------------------Propiedades y Eventos del BOM----------------------------');
window.addEventListener("resize", (e) => {
  console.clear();
  console.log("********** Evento Resize **********");
  // El ancho del viewport
  console.log(window.innerWidth);
  // El alto del viewport
  console.log(window.innerHeight);
   // El ancho exterior del navegador. Todo lo que el navegador mide de ancho
  console.log(window.outerWidth);
  // El alto externo del navegador. Todo lo que el navegador mide de ancho
  console.log(window.outerHeight);
  console.log(e);
});

window.addEventListener("scroll", (e) => {
  console.clear();
  console.log("********** Evento Scroll **********");
   // Qué tan separada está (en pixeles) la parte izquierda del viewport de la parte izquierda del documento html
  console.log(window.scrollX);
  // Qué tan separada está (en pixeles) la parte superior del viewport de la parte superior del documento html
  console.log(window.scrollY);
  console.log(e);
});

window.addEventListener("load", (e) => {
  console.log("********** Evento Load **********");
  // Qué tan separado esta (en pixeles) el borde izquierdo del navegador al border izquierdo de la pantalla de la computadora
  console.log(window.screenX);
  // Qué tan separado esta (en pixeles) la parte superior del navegador del borde superior de la pantalla de la computadora
  console.log(window.screenY);
  console.log(e);
});

document.addEventListener("DOMContentLoaded", (e) => {
  console.log("********** Evento DOMContentLoaded **********");
  console.log(window.screenX);
  console.log(window.screenY);
  console.log(e);
});
// El evento DOMContentLoaded es disparado cuando el documento HTML ha sido completamente cargado y parseado, sin esperar hojas de estilo, imágenes y subtramas para finalizar la carga.
// El evento load se dispara cuando se ha detectado la carga completa de la página.
// Es un error frecuente usar load cuando DOMContentLoaded es mucho más apropiado.
// Peticiones asíncronas pausan el parseo del DOM.

console.log('------------------Métodos del BOM----------------------------');
// window.alert("Alerta");
// window.confirm("Confirmación");
// window.prompt("Aviso");

const $btnAbrir = document.getElementById("abrir-ventana"),
  $btnCerrar = document.getElementById("cerrar-ventana"),
  $btnImprimir = document.getElementById("imprimir-ventana");

let ventana;

$btnAbrir.addEventListener(
  "click",
  (e) => (ventana = window.open("https://google.com"))
);

$btnCerrar.addEventListener("click", (e) => {
  // window.close(); window.close() cierra la pestaña actual
  ventana.close();
});
// window.print() imprime todo el documento html
$btnImprimir.addEventListener("click", (e) => window.print());


console.log('------------------Objetos: URL, Historial y Navegador----------------------------');
console.log("********** Objeto URL (location) **********");
console.log(location);
// Obtiene la ruta que se origina. Actualmente es http://127.0.0.1:5500
console.log(location.origin);
// Obtiene el protocolo. Actulament es http:
console.log(location.protocol);
// Obtiene el host, su nombre. Actualmente es 127.0.0.1:5500. Obtiene sólo el nombre hasta el .<lo-que-sea> como por ejemplo amazon.com.mx
console.log(location.host);
// Obtiene el hostname. Actualmente es 127.0.0.1
console.log(location.hostname);
// Obtiene el puerto. Actualmente es 5500. Veo que algunas veces sólo devuelve un string vacio si el puerto es igual al 80 creo
console.log(location.port);
// Obtiene todo el enlace actual. Actualmente es http://127.0.0.1:5500/dom.html
console.log(location.href);
// Obtiene la parte de la ruta después de un hash, devuelve el # tambien
console.log(location.hash);
// Obtiene la parte de la ruta url que contiene busquedas. Como por ejemplo: ?sku=community&ch=pre&rel=17
console.log(location.search);
// Obtiene la parte de la ruta url despues del hostname pero sin las busquedas como ?sku=community&ch=pre&rel=17
console.log(location.pathname);
// Location reaload() recarga la pagina
//location.reload();

console.log("********** Objeto Historial (history) **********");
console.log(history);
// Longitud del historial
console.log(history.length);
// Ir hacia adelante
//history.forward(1);
// Ir hacia atras o adelante dependiendo de si el numero que se le pasa es negativo o positivo
//history.go(-3);
// Ir hacia atrás
//history.back(2);

console.log("********** Objeto Navegador (navigator) **********");
console.log(navigator);
console.log(navigator.connection);
console.log(navigator.geolocation);
// mediaDevices detecta los dispositivos como camaras, microfonos, etc.
console.log(navigator.mediaDevices);
// mimeTypes trae los mime-types que acepta el navegador.
console.log(navigator.mimeTypes);
// Devuelve un booleano que indica si el usuario está conectado o no a internet
console.log(navigator.onLine);
// Sirve para convertir una aplicación web a Progress Web Apps y para otras cosas más
console.log(navigator.serviceWorker);
console.log(navigator.storage);
console.log(navigator.usb);
console.log(navigator.userAgent);