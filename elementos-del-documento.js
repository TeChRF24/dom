console.log(window.document,'Devuelve todo el documento html con el doctype');
console.log(document);
console.log(document.head,'Devuelve la etiqueta head');
console.log(document.body,'Devuelve la etiqueta body');
console.log(document.documentElement,'Devuelve la etiqueta html sin el doctype');
console.log(document.doctype, 'Trae la etique doctype');
console.log(document.charset, 'Trae el tipo de caracteres, en este caso UTF-8');
console.log(document.title,'Devuelve lo que hay dentro de la etiqueta titulo');
console.log(document.links,'Devuelve todas las etiquetas a');
console.log(document.images, 'Devuelve todas las etiquetas img');
console.log(document.forms,'Devuelve todas las etiquetas form');
console.log(document.styleSheets,'Devuelve todas las etiquetas style');
console.log(document.scripts, 'Devuelve todas las etiquetas script');
setTimeout(() => {
    // getSelection devuelve el texto que se seleccionó en el documento, aplicando toString() devuelve sólo el texto
  console.log(document.getSelection().toString());
  // toString solo devuelve un objeto
  console.log(document.getSelection());
}, 2000);
// write escribe en el documento. Escribe después de todas las etiquetas html que haya
document.write("<h2>Hola Mundo desde el DOM</h2>");